This README will be for Team 5's project 1 repository for calculating pi.

### What is this repository for? ###

* This project is intended to calculate the value of pi using the Monte Carlo method in Python.
* The goal of this project is to calculate an accurate value of pi with minimal amount of error.
* This repository is the source of the collaboration of the contributors in order to successfully create  an accurate value of pi. 
* The contrubutors for this text are:
        Steve Russ
        Alma Stosius
        Lucas Nukaya-Head
        Austin Mello
        Lynn Karriem 
        Brady Garringer
        James Lamb

 [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
=======
* Contributors available for contact:
	steveruss@u.boisestate.edu
	AlmaStosius@u.boisestate.edu
	lucasnukayahead@u.boisestate.edu
	austinmello@u.boisestate.edu
	lynnkarriem@u.boisestate.edu
	bradygarringer@u.boisestate.edu
        jameslamb@u.boisestate.edu 

#MonteCarlo Method
* Measuring a quarter of a circle inscribed in a square 
* finding the ratio of area of the circle to the area square (pi/4) 
* If you throw darts at this area how many dots actually lie within the circle (~78%)* divide your set variable by the number of tests you want to run to find pi
* yields percent error

#Plot Analysis
code yields gaussian distribution  
* Gaussian curve plots 99% and 90% confidence intervals
code yields dartboard that gives a visual of the dart throws 
