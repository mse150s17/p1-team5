#Our Project 1 starter program for calculating pi
import math
import timeit
import numpy
import matplotlib.pyplot as plt

# run time timer start 
T_start = timeit.default_timer() 

#We're going to use "z" to quantize successes
z = 0

#This number entered is how many iterations of 1000 tests.
Test_Number_Input = input("(1,000 recommended) How many tests would you like to run?: ")
pi_v_length = int(Test_Number_Input)
pi_vector = numpy.zeros(pi_v_length)


#We want to calculate pi using the Monte Carlo Method
#Throw darts at a square dartboard
#Count darts that go in circle vs total number of darts = pi/4


#Need ability to throw darts at a square
#Darts will be throw in the 1st quadrant only
def throw_dart():
	x = numpy.random.random() #Each of these should be between 0 and 1
	y = numpy.random.random()
	return x,y

	
#Need ability to determine if the dart is in the circle
#0 is outside of circle
#1 is inside the circle
def in_circle(point): 
	d = (point[0]**2 + point[1]**2)**(0.5)
	if d > 1.0:
		return 0
	return 1


#This for loop is ran x amount of times based on the integer entered by the user.
Tests=1000
for j in range (pi_v_length):
	z = 0
	for i in range (Tests):
		if (in_circle(throw_dart())) == 1:
			z+=1
	pi_vector[j]=4*z/Tests

#The ratio of successes to tests should be pi/4. So if we do 4*z / tests, the
#result is appx. pi 
pi = numpy.mean(pi_vector)


#Making the printout of pi more descriptive
print( " ")
print( "The calculated value for pi is:", +pi)
print( " ")


#Finding the percent error for the actual value of pi vs the calculated value of pi
actualPi = numpy.pi
error = (abs(pi - actualPi)/actualPi)*100
print( "Percent error vs actual pi: ", +error, '%')
print( " ")


#-----------------------------------------------(stats vvv)
#Used for rounding in legend, and clock
def number_rounding(x, n):
	if not x: return 0
	power = -int(math.floor(math.log10(abs(x)))) + (n-1)
	factor = (10 ** power)
	return round(x * factor) / factor

#(these three lines reconcile my work on my computer with our code) -Brady
statsPak = pi_vector
statsPakLength = pi_v_length
tests = Tests

#----------------------------------------------(Plotting)
#left subplot
plt.subplot(121)

#plots the Gaussian Distribution
t = numpy.arange(1,5,.00001)
plt.plot(t, (numpy.e)**((-(t-numpy.mean(statsPak))**2)/(2*numpy.var(statsPak))) / (2*numpy.pi*numpy.var(statsPak))**.5, 'r.')

#This is the mean, in this case: Pi
plt.axvline(numpy.mean(statsPak) , 0 , 4 , 1 ,color='k')
plt.annotate(r'$\pi \approx$' +str(numpy.average(statsPak)),(numpy.mean(statsPak),.1))

#90% confidence interval
NinetyUpper = numpy.mean(statsPak) + 1.645*numpy.std(statsPak)/numpy.sqrt(statsPakLength)  
NinetyLower = numpy.mean(statsPak) - 1.645*numpy.std(statsPak)/numpy.sqrt(statsPakLength)  

#99% confidence interval
NNUpper = numpy.mean(statsPak) + 2.58*numpy.std(statsPak)/numpy.sqrt(statsPakLength)  
NNLower = numpy.mean(statsPak) - 2.58*numpy.std(statsPak)/numpy.sqrt(statsPakLength)  

#used for the legend
NNInterval = str('99% confidence interval: ') + str(number_rounding((NNLower),4)) + ' , ' + str(number_rounding((NNUpper),4))
NinetyInterval = str('90% confidence interval: ') + str(number_rounding((NinetyLower),4)) + ' , ' + str(number_rounding((NinetyUpper),4))

#legend
plt.axvline(NNUpper, .25 , 4 , 1 ,color='r', label=NNInterval)
plt.axvline(NNLower, .25 , 4 , 1 ,color='r')
plt.axvline(NinetyUpper, .15 , 4 , 1 ,color='b', label=NinetyInterval)
plt.axvline(NinetyLower, .15 , 4 , 1 ,color='b')
plt.legend()

#set limits of graph
plt.ylim(0,(tests/100))
#change x-limit to see more of the Gaussian curve
plt.xlim(3,3.31)
plt.title("Gaussian Distribution of Results")
plt.xlabel("Calculated values of Pi")

#Plots 1 iteration of 1000 dart throws in the right subplot
plt.subplot(122)
for q in range(tests):
	A = throw_dart()
	if A[0]<=numpy.sqrt(1-A[1]**2):
		plt.scatter(A[0],A[1],15,c='b')
	else:
		plt.scatter(A[0],A[1],15,c='y')

#sets x and y limits
plt.xlim(0,1)
plt.ylim(0,1)
#plots a quarter circle on the data
v = numpy.arange(0,1,.0001)
plt.plot(v , numpy.sqrt(1 - v**2) ,'r')

#Timer stop
elapsed = timeit.default_timer() - T_start
print("Program run time was:",(number_rounding((elapsed),3)),"seconds")

#show the plot
plt.show()
